CREATE TABLE `user_topic_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `topic_id` INT UNSIGNED NOT NULL COMMENT '话题id',
  `used_times` INT UNSIGNED NOT NULL DEFAULT 1 COMMENT '话题使用次数',
  `last_use_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '话题最后使用时间',
  PRIMARY KEY `id_pk` (`id`),
  UNIQUE INDEX `ids1_uq` (`user_id`, `topic_id`),
  INDEX `lastUseTime_index` (`last_use_time`)
) ENGINE=MYISAM COMMENT='用户话题使用记录表,为了方便给用户展示最近使用过的或者经常使用的话题' CHARSET=utf8 COLLATE=utf8_general_ci;